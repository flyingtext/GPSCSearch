﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;

namespace GPSCSearch
{
    public class Runner
    {
        public List<BigInteger> Primes;
        public BigInteger TPCnt = 0;
        public BigInteger TNCnt = 0;
        public BigInteger FPCnt = 0;
        public BigInteger FNCnt = 0;

        public Runner(List<BigInteger> LastRun)
        {
            if (LastRun == null)
            {
                this.Primes = new List<BigInteger>() { 2, 3, 5, 7, 11 };
            }
            else if (LastRun.Count == 0)
            {
                this.Primes = new List<BigInteger>() { 2, 3, 5, 7, 11 };
            }
            else
            {
                this.Primes = LastRun;
            }
        }

        BigInteger GetNMultiplier(BigInteger Nth)
        {
            return 9 * BigInteger.Pow(Nth, 3) - 3 * BigInteger.Pow(Nth, 2);
        }

        BigInteger GetMaxNMultiplier(BigInteger n)
        {
            BigInteger Cnt = 0;
            while(true)
            {
                if(n < this.GetNMultiplier(Cnt))
                {
                    return Cnt - 1;
                }
                else
                {
                    Cnt = Cnt + 1;
                }
            }
        }

        BigInteger GetECF(List<BigInteger> Mul, BigInteger Plus)
        {
            BigInteger Ret = 0;
            BigInteger Cnt = 0;
            foreach(BigInteger M in Mul)
            {
                Ret = Ret + this.GetNMultiplier(Cnt) * M;
                Cnt = Cnt + 1;
            }
            return Ret + Plus;
        }

        bool PrimeChecker(BigInteger n)
        {
            List<BigInteger> NewPrimes = new();

            foreach(BigInteger p in this.Primes)
            {
                if(BigInteger.Remainder(n, p) == 0)
                {
                    return false;
                }
                if(BigInteger.Remainder(p, n) != 0)
                {
                    NewPrimes.Add(p);
                }
            }

            this.Primes = NewPrimes;
            return true;
        }



        bool SqrtPrimeChecker(BigInteger n)
        {
            
            for (int i = 3; i < ((int)Math.Exp(BigInteger.Log(n) / 2)); i += 2)
            {
                if(BigInteger.Remainder(n, i) == 0)
                {
                    return false;
                }
            }
            return true;
        }

        public void RecursivePrime(List<BigInteger> Mul, BigInteger Depth, BigInteger Limit, bool InnerSqrtVerify, bool OuterSqrtVerify, bool LastBigMode, BigInteger LastBig, bool Verbose)
        {
            if(Depth == 1)
            {
                return;
            }
            BigInteger LastPrimeCount = 0;
            bool Flag = false;
            List<BigInteger> GeneratedPrimes = new() { 2 };
            while(LastPrimeCount != GeneratedPrimes.Count)
            {
                LastPrimeCount = GeneratedPrimes.Count;
                if (Flag)
                {
                    break;
                }
                foreach(BigInteger p in this.Primes)
                {
                    if(Flag)
                    {
                        break;
                    }
                    if(Mul[Mul.Count-1] > p)
                    {
                        continue;
                    }
                    List<BigInteger> CopyMul = Enumerable.Concat(Mul, new List<BigInteger>() { p }).ToList();

                    foreach(int Plus in new List<int>() { 1, 3, 5 } )
                    {
                        BigInteger Calculated = this.GetECF(CopyMul, Plus);
                        if(Calculated > Limit)
                        {
                            Flag = true;
                            break;
                        }
                        if(LastBigMode)
                        {
                            if(Calculated < LastBig)
                            {
                                continue;
                            }
                        }

                        if(Calculated == 1)
                        {
                            continue;
                        }

                        if(this.Primes.IndexOf(Calculated) != -1)
                        {
                            continue;
                        }
                        bool PrimeTF = this.PrimeChecker(Calculated);

                        if(PrimeTF)
                        {
                            if(InnerSqrtVerify)
                            {
                                bool SPCTF = this.SqrtPrimeChecker(Calculated);
                                if(SPCTF)
                                {
                                    this.TPCnt = this.TPCnt + 1;
                                }
                                else
                                {
                                    this.FPCnt = this.FPCnt + 1;
                                }
                                if(Verbose)
                                {
                                    Console.WriteLine("{0} / {1} / [{2}, {3}] / TP {4} / FP {5} / TN {6} / FN {7} / TP+FN {8} / Accuracy {9}%",
                                        Calculated.ToString(),
                                        SPCTF.ToString(),
                                        Plus.ToString(),
                                        string.Join(", ", CopyMul.Skip(1).ToArray()),
                                        this.TPCnt, this.FPCnt, this.TNCnt, this.FNCnt, this.TPCnt + this.FNCnt,
                                        Math.Round((float)((this.TPCnt + this.FNCnt) * 10000 / (this.TPCnt + this.TNCnt + this.FPCnt + this.FNCnt))) / 100
                                        );
                                    if(SPCTF)
                                    {
                                        this.Primes.Add(Calculated);
                                        GeneratedPrimes.Add(Calculated);
                                    }
                                }
                            }
                            else
                            {
                                this.Primes.Add(Calculated);
                                GeneratedPrimes.Add(Calculated);
                                if(Verbose)
                                {
                                    Console.WriteLine("{0} / [{1}, {2}]", Calculated,
                                        Plus.ToString(),
                                        string.Join(", ", CopyMul.Skip(1).ToArray()));
                                }
                            }
                        }
                        else
                        {
                            if(OuterSqrtVerify)
                            {
                                bool SPCTF = this.SqrtPrimeChecker(Calculated);
                                if(SPCTF)
                                {
                                    this.TNCnt = this.TNCnt + 1;
                                    this.Primes.Add(Calculated);
                                    GeneratedPrimes.Add(Calculated);
                                }
                                else
                                {
                                    this.FNCnt = this.FNCnt + 1;
                                }
                            }
                        }
                    }
                }
            }

            foreach(BigInteger p in this.Primes)
            {
                List<BigInteger> CopyMul = Enumerable.Concat(Mul, new List<BigInteger>() { p }).ToList();
                if (this.GetECF(CopyMul, 5) > Limit)
                {
                    break;
                }
                if(Mul[Mul.Count-1] >= p)
                {
                    continue;
                }
                if(LastBigMode)
                {
                    this.RecursivePrime(CopyMul, Depth - 1, Limit, InnerSqrtVerify, OuterSqrtVerify, LastBigMode, Primes.Max(), Verbose);
                }
                else
                {
                    this.RecursivePrime(CopyMul, Depth - 1, Limit, InnerSqrtVerify, OuterSqrtVerify, LastBigMode, -1, Verbose);
                }
            }
        }
    }
    public class Run
    {
        public Run()
        {

        }

        public void GPSCSearch(bool InnerSqrtVerify, bool OuterSqrtVerify, bool LastBigMode, bool VerifyResult, bool VerifyMax, int Alpha, bool Verbose, BigInteger Limit, bool Save, List<BigInteger> LastResult)
        {
            Runner runner = new(LastResult);
            runner.RecursivePrime(new List<BigInteger>() { 0 }, Alpha, Limit, InnerSqrtVerify, OuterSqrtVerify, LastBigMode, -1, Verbose);
        }
    }
    
    class Program
    {
        static void Main(string[] args)
        {
            Run run = new();
            run.GPSCSearch(true, false, false, true, true, 3, true, 100000000000, true, new List<BigInteger>() { });
        }
    }
    
}

