import click
import math
import time

class Runner :
    primes = [2, 3, 5, 7, 11]
    tp_cnt = 0
    tn_cnt = 0
    fp_cnt = 0
    fn_cnt = 0

    def __init__(self, initial_primes) :
        if len(initial_primes) != 0 :
            self.primes = initial_primes

    def get_n_multiplier(self, nth) :
        return 9*math.pow(nth, 3) - 3*math.pow(nth, 2)

    def get_max_n_multiplier(self, n) :
        cnt = 0
        while True:
            if n < self.get_n_multiplier(cnt) :
                return cnt - 1
            else :
                cnt = cnt + 1

    def get_ecf(self, mul, plus) :
        ret = 0
        for i, m in enumerate(mul) :
            ret = ret + self.get_n_multiplier(i) * m
        return ret + plus

    def prime_checker(self, n) :
        copy_dividers = []
        for i, p in enumerate(self.primes) :
            if (n % p) == 0 :
                return False
            if (p % n) != 0 :
                copy_dividers.append(p)
        self.primes = copy_dividers
        return True

    def sqrt_prime_checker(self, n) :
        for i in range(2, int(math.sqrt(n) + 1)) :
            if (i % 2) == 0 :
                continue
            if (n % i) == 0:
                return False
        return True

    def recursive_prime(self, lis, depth, limit, inner_sqrt_verify, outer_sqrt_verify, last_big_mode, last_big, verbose) :
        if depth == 1 :
            return
        last_p_count = 0
        flag = False
        generated_primes = [2]
        while last_p_count != len(generated_primes) :
            last_p_count = len(generated_primes)
            if flag :
                break
            for ind, p in enumerate(self.primes) :
                if flag :
                    break
                if lis[-1] > p :
                    continue
                copy_lis = lis + [p]
                for en in [1, 3, 5]: 
                    calc = int(self.get_ecf(copy_lis, en))
                    if calc > limit :
                        flag = True
                        break
                    if last_big_mode :
                        if calc < last_big :
                            continue
                    if calc == 1 :
                        continue
                    if calc in self.primes :
                        continue
                    prime_tf = self.prime_checker(calc)
                    if prime_tf :
                        if inner_sqrt_verify :
                            spc = self.sqrt_prime_checker(calc)
                            if spc == True :
                                self.tp_cnt = self.tp_cnt + 1
                            else :
                                self.fp_cnt = self.fp_cnt + 1
                            if verbose :
                                print(calc, spc, [en] + copy_lis[1:], '/ TP', self.tp_cnt, '/ FP', self.fp_cnt, 
                                '/ TN', self.tn_cnt, '/ FN', self.fn_cnt, '/ TP+FN', self.tp_cnt+self.fn_cnt, '/ Accuracy',
                                str(round(((self.tp_cnt + self.fn_cnt) / (self.tp_cnt + self.fp_cnt + self.tn_cnt + self.fn_cnt)) * 10000) / 100) + '%', end='\r')
                            if spc :
                                self.primes.append(calc)
                                generated_primes.append(calc)
                        else :
                            self.primes.append(calc)
                            generated_primes.append(calc)
                            if verbose :
                                print(calc, [en] + copy_lis[1:], end='\r')
                    else :
                        if outer_sqrt_verify :
                            spc = self.sqrt_prime_checker(calc)
                            if spc == True :
                                self.tn_cnt = self.tn_cnt + 1
                            else :
                                self.fn_cnt = self.fn_cnt + 1
                            if spc :
                                self.primes.append(calc)
                                generated_primes.append(calc)
        for p in self.primes :
            if self.get_ecf(lis+[p], 5) > limit :
                break
            if lis[-1] >= p :
                continue
            if last_big_mode :
                ret = self.recursive_prime(lis + [p], depth - 1, limit, inner_sqrt_verify, outer_sqrt_verify, last_big_mode, max(generated_primes), verbose)
            else :
                ret = self.recursive_prime(lis + [p], depth - 1, limit, inner_sqrt_verify, outer_sqrt_verify, last_big_mode, None, verbose)

@click.command()
@click.option('--inner-sqrt-verify', default=True, help='True or false to verify the result from gpsc with square root method (default true)')
@click.option('--outer-sqrt-verify', default=False, help='True or false to verify the false result from gpsc with square root method (default false)')
@click.option('--last-big-mode', default=True, help='True or false to use last big mode which would tend less numbers but big result (default false)')
@click.option('--verify-result', default=True, help='Verify results with square root method (default true)')
@click.option('--verify-max', default=True, help='Verify max value with square root method (default true)')
@click.option('--alpha', default=0, help='Number of maximum alpha counts (default 0 for auto mode)')
@click.option('--verbose', default=True, help='Set false for quiet mode (default true)')
@click.option('--save', default=True, help='Save result (default true)')
@click.option('--last-result', default='', help='File path to last result to speed up search with last search result')
@click.argument('limit', default=10000)
def app(inner_sqrt_verify, outer_sqrt_verify, last_big_mode, verify_result, verify_max, alpha, verbose, limit, save, last_result) :
    filename = str(int(time.time() * 1000000))

    last_run = []
    if last_result != '' :
        fp = None
        try :
            fp = open(last_result, 'r', encoding='utf-8')
        except Exception as e:
            print(e, '/ File open error')
            exit()
        data = fp.read().split('\n')[1:]

        for l in data :
            try :
                last_run.append(int(l.split(',')[0]))
            except :
                print('Passing read line /', l)
        fp.close()
    if save :
        fp = None
        try :
            fp = open(filename + '.txt', 'w', encoding='utf-8')
        except Exception as e:
            print(e, '/ File open error')
            exit()
        fp.close()
    runner = Runner(last_run)
    start = time.time()
    if alpha == 0 :
        alpha = int(math.log(runner.get_max_n_multiplier(limit), 60))
    runner.recursive_prime([0], alpha+1, limit, inner_sqrt_verify, outer_sqrt_verify, last_big_mode, 2, verbose)
    end = time.time()
    print('')
    max_val = None
    is_max_val_prime = None
    if inner_sqrt_verify :
        if verify_max :
            max_val = max(runner.primes)
            is_max_val_prime = runner.sqrt_prime_checker(max_val)
            print('Max value', max_val, '/ Prime =', is_max_val_prime)
        else :
            print('Max value', max(runner.primes))
        print('GPSC Time elapsed /', end - start, '/ TP', runner.tp_cnt, '/ FP', runner.fp_cnt, 
            '/ TN', runner.tn_cnt, '/ FN', runner.fn_cnt, '/ TP+FN', runner.tp_cnt+runner.fn_cnt, '/ Accuracy',
            str(round(((runner.tp_cnt + runner.fn_cnt) / (runner.tp_cnt + runner.fp_cnt + runner.tn_cnt + runner.fn_cnt)) * 10000)/100) + '%')
    else :
        max_val = max(runner.primes)
        is_max_val_prime = runner.sqrt_prime_checker(max_val)
        print('Max value', max_val, '/ Prime =', is_max_val_prime)
        print('GPSC Time elapsed /', end - start)
    
    verify_res = []

    if verify_result :
        true_cnt = 0
        false_cnt = 0
        start = time.time()
        for i in runner.primes :
            if i % 2 == 0 :
                false_cnt = false_cnt + 1
                verify_res.append(False)
                continue
            flag = True
            for j in range(3, i, 2) :
                if (i%j)==0 :
                    flag = False
                    false_cnt = false_cnt + 1
                    verify_res.append(False)
                    break
            if flag :
                true_cnt = true_cnt + 1
                verify_res.append(True)
        end = time.time()

        print('Square Root Final Verification Time elapsed /', end - start, '/ Correct result', true_cnt, '/ Wrong result', false_cnt, '/ Total count', true_cnt + false_cnt)

    if save :

        fp = None
        try :
            fp = open(filename + '.txt', 'w', encoding='utf-8')
        except Exception as e:
            print(e, '/ File open error')
        fp.write('searched_max_value,')
        fp.write(str(max_val) + '\n')
        if verify_result :
            for i, p in enumerate(runner.primes) :
                fp.write(str(p) + ',' + str(verify_res[i]) + '\n')
        else :
            for i, p in enumerate(runner.primes) :
                fp.write(str(p) + '\n')
        fp.close()
    return runner.primes


if __name__ == '__main__' :
    app()