# GPSCSearch [![DOI](https://zenodo.org/badge/616815560.svg)](https://zenodo.org/badge/latestdoi/616815560)
Prime number search library based on Generalized Prime Sequence Conjecture(GPSC)

## Introduction
Based on Generalized Prime Sequence Conjecture(GPSC, [TechRxiv](https://doi.org/10.36227/techrxiv.22306411), [Zenodo](https://doi.org/10.5281/zenodo.7754381)), GPSCSearch is a library that specialized for searching prime number.

## What is Generalized Prime Sequence Conjecture(GPSC)?
Generally speaking, prime number is defined like following:

$$
p \in \mathbb{N}\ is\ \textbf{prime} \iff \nexists n \in \mathbb{N}\ s.t.\ n\mid p\ and\ n\ne p, 1.
$$

It's conjecture that every prime takes a form like following:

$$
\begin{aligned}
& M_n = 9n^3-3n^2 \Longrightarrow \forall p \geq 3 \\
& \Longrightarrow p_n = k + \alpha_{1} M_{1} + \alpha_{2} M_{2} + \alpha_{3} M_{3} \cdots \\
& (\forall \alpha \in \mathbb{N}\ is\ prime, k \in \{0, 1, 3, 5\})
\end{aligned}
$$

By adding search algorithm to the multiplier $M_n$ and its prime parameter $\alpha$, target $p_n$ could be caculated. More detailed method would be added later.

## Setup

Clone this repository and setup requirements.

```
git clone https://github.com/flyingtext/GPSCSearch.git
pip install -r requirements.txt
```

To see help:

```
python gpsc.py --help
```

## Recommended usage

Firstly, run below to gather basic primes. The results would be saved as a text file with name in timestamp-based numbers.

```
python gpsc.py --alpha=2 --inner-sqrt-verify=true --verify-result=false 10000000000
```

Next, run below to get large number of prime with reasonable approach. (1679433086957144.txt file is just an example.)

```
python gpsc.py --alpha=12 --last-big-mode=true --inner-sqrt-verify=true --verify-result=false --last-result=1679433086957144.txt 1000000000000000
```

## Strong warning

Any attempt to use this conjecture or program in order to harm public interest would be charged for their own by their responsibilities.

## Update log

* 2023/03/22 - First release

## Links

* Blog [flyingtext](https://flyingtext.blog)
* E-mail [flyingtext@nate.com](flyingtext@nate.com)
* ORCID [0000-0001-9610-0994](https://orcid.org/0000-0001-9610-0994)
* [TechRxiv](https://www.techrxiv.org/authors/Jihyeon_Yoon/13214070), [SSRN](https://papers.ssrn.com/sol3/cf_dev/AbsByAuth.cfm?per_id=5334925)

## License

This program is licensed under GPL 3.0.